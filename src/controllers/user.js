'use strict';
const mongoose = require('mongoose');
const User = mongoose.model('User');
const authService = require('../services/authService');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);

exports.findAll = (req, res, next) => {
	User.find({}).then(x => {
		res.status(200).send({ total: x.length, data: x });
	}).catch(e => {
		res.status(400).send({ message: 'Falha na busca.', data : e });
		res.status(404).send({ message: 'Nenhum registro encontrado.' });
	});
};

exports.findOneById = (req, res, next) => {
	User.findById(req.params.id).then(x => {
		res.status(200).send({ message: 'Contato encontrado.', data: x });
	}).catch(e => {
		res.status(400).send({ message: 'Falha na busca.', data : e });
		res.status(404).send({ message: 'Nenhum registro encontrado.' });
	});
};

exports.post = (req, res, next) => {
	var user = new User({
		username: req.body.username,
		password: bcrypt.hashSync(req.body.password, salt),
	});
	user.save().then(x => {
		res.status(201).send({ message: 'Cadastrado com Sucesso!', data: x });
	}).catch(e => {
		res.status(400).send({ message: 'Erro ao cadastrar.', data : e });
	});
};

exports.put = (req, res, next) => {
	User.findByIdAndUpdate(req.params.id, { 
		$set: {
			username: req.body.username,
			password: req.body.password
		}
 	}).then(x => {
		res.status(200).send({ message: { type: 'ok', description :'Contato atualizado.' }, data: x });
	}).catch(e => {
		res.status(400).send({ message: 'Falha na atualização.', data : e });
		res.status(404).send({ message: 'Nenhum registro encontrado.' });
	});
};

exports.authenticate = (req, res, next) => {
	User.find({
		username: req.body.username
	}).then(x => {
		if (x.length > 0 && bcrypt.compareSync(req.body.password, x[0].password)){
			const token = jwt.sign({sub: req.body.username}, 'sgãsdjã-ewr423fd-fsdqwdfd', { expiresIn: '1h' });
			res.status(200).send({ message: {description: 'Credenciais corretas', type: 'success'}, usuario: x[0].username, data: token });
		} else {
			res.status(200).send({ message: {description: 'Erro nas credenciais', type: 'danger'}})
		}
	}).catch(e => {
		res.status(401).send({ message: 'Erro no login.', data : e });
	});
};