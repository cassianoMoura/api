'use strict';
const mongoose = require('mongoose');
const Contact = mongoose.model('Contact');

exports.findAll = (req, res, next) => {
	Contact.find({active: true}).then(x => {
		res.status(200).send({ message: { type: 'success', description: 'Sucesso.'}, total: x.length, data: x });
	}).catch(e => {
		res.status(400).send({ message: { type: 'danger', description: 'Falha na busca.'}, data: e });
	});
};

exports.findOneById = (req, res, next) => {
	Contact.findById(req.params.id).then(x => {
		res.status(201).send({ message: { type: 'success', description: 'Contato encontrado.'}, data: x });
	}).catch(e => {
		res.status(404).send({ message: { type: 'danger', description: 'Erro ao buscar contato.'}, data: e });
	});
};

exports.post = (req, res, next) => {
	var contact = new Contact(req.body);
	contact.save().then(x => {
		res.status(201).send({ message: { type: 'success', description: 'Contato cadastrado.'}, data: x });
	}).catch(e => {
		res.status(400).send({ message: { type: 'danger', description: 'Erro ao cadastrar.'}, data: e });
	});
};

exports.put = (req, res, next) => {
	Contact.findByIdAndUpdate(req.params.id, { 
		$set: {
			name: req.body.name,
			address: req.body.address,
			phone: req.body.phone,
			mail: req.body.mail,
			active: req.body.active
		}
 	}).then(x => {
		res.status(200).send({ message: { type: 'success', description: 'Contato atualizado.'}, data: x });
	}).catch(e => {
		res.status(400).send({ message: { type: 'danger', description: 'Erro ao atualizar.'}, data: e });
	});
};
