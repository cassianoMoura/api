'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/user');
const authService = require('../services/authService');

router.post('/login', controller.authenticate);

router.get('/users', authService.authorize, controller.findAll);

router.post('/users', authService.authorize, controller.post);

router.get('/users/:id', authService.authorize, controller.findOneById);

router.put('/users/:id', authService.authorize, controller.put);

module.exports = router;