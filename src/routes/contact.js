'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controllers/contact');
const authService = require('../services/authService');

router.post('/contacts', authService.authorize, controller.post);

router.get('/contacts', authService.authorize, controller.findAll);

router.get('/contacts/:id', authService.authorize, controller.findOneById);

router.put('/contacts/:id', authService.authorize, controller.put);

module.exports = router;